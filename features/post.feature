Feature: Post generator
  In order to generate a post
  As a CLI newbie
  I want postmachine to hold my hand, tightly

  Scenario: Posts
    When I run `postmachine create post`
    Then the following files should exist:
      | _posts/post.markdown |
    Then the file "_posts/post.markdown" should contain:
      """
      ---
      layout: post
      title: post
      ---
      """
