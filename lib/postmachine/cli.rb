require 'thor'
require 'postmachine/generators/post'

module Postmachine
  class CLI < Thor

    desc "create", "Generates a post scaffold"
    def create(name)
      Postmachine::Generators::Post.start([name])
    end

  end
end
