require 'thor/group'
module Postmachine
  module Generators
    class Post < Thor::Group
      include Thor::Actions

      argument :name, :type => :string

      def self.source_root
        File.dirname(__FILE__) + "/templates"
      end


      def create_group
        empty_directory("_posts")
      end

      def create_post
        template("post.markdown", "_posts/#{name}.markdown")
      end
    end
  end
end
